export enum UserRole {
  SYSTEM = 'SYSTEM',
  ADMIN = 'ADMIN',
  REGULAR = 'REGULAR',
  SUPER_ADMIN = 'SUPER_ADMIN',
}