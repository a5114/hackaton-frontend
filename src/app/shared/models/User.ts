import { UserRole } from "../enums/user-role.enum";

export class User {
  public id?: number;
  public name?: string;
  public role?: UserRole;

  constructor() {
  }
}