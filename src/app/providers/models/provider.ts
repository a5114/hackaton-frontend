export interface IProvider {
    id: number,
    name: string,
    cellphone: string,
    company: string
}