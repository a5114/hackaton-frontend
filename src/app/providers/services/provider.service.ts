import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProvider } from '../models/provider';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  private url = 'http://localhost:3000/providers';

  constructor(private httpClient: HttpClient) { }

  getProvidersList(): Observable<IProvider[]> {
    return this.httpClient.get<IProvider[]>(this.url).pipe(
      map((data) => data)
    );
  }

  getProviderById(id: number): Observable<IProvider> {
    return this.httpClient.get<IProvider>(this.url + "/" + id).pipe(
      map((data) => data)
    );
  }

  addProvider(provider: IProvider) {
    return this.httpClient.post(this.url, provider);
  }

  editProvider(provider: IProvider, id: number) {
    return this.httpClient.put(this.url + "/" + id, provider);
  }

  deleteProvider(id: number) {
    return this.httpClient.delete(this.url + "/" + id);
  }
}
