import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../core/guards/role.guard';
import { UserRole } from '../shared/enums/user-role.enum';
import { ProviderEditFormComponent } from './components/provider-edit-form/provider-edit-form.component';
import { ProviderFormComponent } from './components/provider-form/provider-form.component';
import { ProvidersListComponent } from './components/providers-list/providers-list.component';

const routes: Routes = [
  {
    path: '',
    component: ProvidersListComponent,
    canActivate: [RoleGuard],
    data: {
      role: [
        UserRole.ADMIN
      ]
    }
  },
  {
    path: 'new-provider',
    component: ProviderFormComponent,
    canActivate: [RoleGuard],
    data: {
      role: [
        UserRole.ADMIN
      ]
    }
  },
  {
    path: 'edit-provider/:providerId',
    component: ProviderEditFormComponent,
    canActivate: [RoleGuard],
    data: {
      role: [
        UserRole.ADMIN
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProvidersRoutingModule { }
