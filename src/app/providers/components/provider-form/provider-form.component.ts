import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProvider } from '../../models/provider';
import { ProviderService } from '../../services/provider.service';

@Component({
  selector: 'app-provider-form',
  templateUrl: './provider-form.component.html',
  styleUrls: ['./provider-form.component.css']
})
export class ProviderFormComponent implements OnInit {
  providerForm!: FormGroup;

  constructor(private providerService: ProviderService, private router: Router, private builder: FormBuilder,
              private toast: ToastrService) { }

  ngOnInit(): void {
    this.providerForm = this.builder.group({
      name: ['', Validators.required],
      cellphone: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(8)])],
      company: ['', Validators.required]
    });
  }

  addProvider() {
    const provider: IProvider = this.providerForm.value;
    this.providerService.addProvider(provider).subscribe(resp => {      
      this.toast.success('Successfully created provider', '');
      this.router.navigate(['/app/providers']);
    });
  }

  get name(): FormControl {
    return this.providerForm.get('name') as FormControl;
  }
  get cellphone(): FormControl {
    return this.providerForm.get('cellphone') as FormControl;
  }
  get company(): FormControl {
    return this.providerForm.get('company') as FormControl;
  }
}
