import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { IProvider } from '../../models/provider';
import { ProviderService } from '../../services/provider.service';

@Component({
  selector: 'app-providers-list',
  templateUrl: './providers-list.component.html',
  styleUrls: ['./providers-list.component.css']
})
export class ProvidersListComponent implements OnInit {
  list!: IProvider[]
  constructor(private providerService: ProviderService, private toast: ToastrService) { }

  ngOnInit(): void {
    this.listProviders();
  }

  listProviders() {
    this.providerService.getProvidersList().subscribe(data => {
      this.list = data;
    });
  }

  deleteProvider(id: number) {
    var r = confirm ("Are you sure to remove the provider?");
    if (r == true) {
      this.providerService.deleteProvider(id).subscribe(resp => {
        this.toast.success('Provider successfully removed', '');
        this.listProviders();
      });
    }
  }
}
