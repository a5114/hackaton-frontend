import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProvider } from '../../models/provider';
import { ProviderService } from '../../services/provider.service';

@Component({
  selector: 'app-provider-edit-form',
  templateUrl: './provider-edit-form.component.html',
  styleUrls: ['./provider-edit-form.component.css']
})
export class ProviderEditFormComponent implements OnInit {
  provider!: IProvider;
  providerId !: number;
  providerForm!: FormGroup;

  constructor(private providerService: ProviderService, private router: Router, private builder: FormBuilder,
              private route: ActivatedRoute, private toast: ToastrService) { }

  ngOnInit(): void {    
    const routeParams = this.route.snapshot.paramMap;
    this.providerId = Number(routeParams.get('providerId'));
    this.providerForm = this.builder.group({
      name: ['', Validators.required],
      cellphone: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(8)])],
      company: ['', Validators.required]
    });

    if (this.providerId != null) {
      this.providerService.getProviderById(this.providerId).subscribe(resp => {
        this.provider = resp;
        this.loadData();
      });
    }
  }

  editProvider() {
    const provider: IProvider = this.providerForm.value;
    this.providerService.editProvider(provider, this.providerId).subscribe(resp => {      
      this.toast.success('Provider successfully edited', '');
      this.router.navigate(['/app/providers']);
    });
  }

  loadData() {
    this.name.setValue(this.provider.name);
    this.cellphone.setValue(this.provider.cellphone);
    this.company.setValue(this.provider.company);
  }

  get name(): FormControl {
    return this.providerForm.get('name') as FormControl;
  }
  get cellphone(): FormControl {
    return this.providerForm.get('cellphone') as FormControl;
  }
  get company(): FormControl {
    return this.providerForm.get('company') as FormControl;
  }
}
