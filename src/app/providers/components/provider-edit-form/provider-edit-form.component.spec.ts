import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderEditFormComponent } from './provider-edit-form.component';

describe('ProviderEditFormComponent', () => {
  let component: ProviderEditFormComponent;
  let fixture: ComponentFixture<ProviderEditFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProviderEditFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
