import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProvidersRoutingModule } from './providers-routing.module';
import { ProvidersListComponent } from './components/providers-list/providers-list.component';
import { ProviderEditFormComponent } from './components/provider-edit-form/provider-edit-form.component';
import { ProviderFormComponent } from './components/provider-form/provider-form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProvidersListComponent,
    ProviderEditFormComponent,
    ProviderFormComponent
  ],
  imports: [
    CommonModule,
    ProvidersRoutingModule,
    ReactiveFormsModule
  ]
})
export class ProvidersModule { }
