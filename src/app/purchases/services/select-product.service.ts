import { Injectable } from '@angular/core';
import {TemporalOrder} from "../models/temporal-order";

@Injectable({
  providedIn: 'root'
})
export class SelectProductService {

  _selectedProduct : TemporalOrder;

  constructor() { }

  public get selectedProduct(){
    return this._selectedProduct;
  }

  public set selectedProduct(list : TemporalOrder){
    this._selectedProduct = list;
  }

}
