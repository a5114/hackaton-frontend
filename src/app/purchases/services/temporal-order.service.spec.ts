import { TestBed } from '@angular/core/testing';

import { TemporalOrderService } from './temporal-order.service';

describe('OrderService', () => {
  let service: TemporalOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TemporalOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
