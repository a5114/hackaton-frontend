import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {TemporalOrder} from "../models/temporal-order";

@Injectable({
  providedIn: 'root'
})
export class TemporalOrderService {
  url : string = 'http://localhost:3000/temporal_orders'

  constructor(private httpClient : HttpClient) { }

  post(order :TemporalOrder): Observable<TemporalOrder>{
    return this.httpClient.post<TemporalOrder>(this.url,order);
  }

  getByUserId(userId: number): Observable<TemporalOrder[]>{
    return this.httpClient.get<TemporalOrder[]>(this.url+"?userId="+userId);
  }

  delete(id:string){
    return this.httpClient.delete(this.url+"/"+id);
  }


}
