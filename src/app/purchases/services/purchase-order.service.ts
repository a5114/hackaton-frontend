import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {PurchaseOrder} from "../models/purchase-order";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderService {
  url : string = 'http://localhost:3000/purchase_orders'

  constructor(private httpClient : HttpClient) { }

  post(purchaseOrder : PurchaseOrder): Observable<PurchaseOrder>{
    return this.httpClient.post<PurchaseOrder>(this.url,purchaseOrder);
  }
  getPurchaseOrderList(): Observable<PurchaseOrder[]> {
    return this.httpClient.get<PurchaseOrder[]>(this.url).pipe(
      map((data) => data)
    );
  }

  getPurchaseOrderById(id: number): Observable<PurchaseOrder> {
    return this.httpClient.get<PurchaseOrder>(this.url + "/" + id).pipe(
      map((data) => data)
    );
  }
  editPurchaseOrder(purchase_order: PurchaseOrder, id: number) {
    return this.httpClient.put(this.url + "/" + id, purchase_order);
  }

  deleteProvider(id: number) {
    return this.httpClient.delete(this.url + "/" + id);
}
}
