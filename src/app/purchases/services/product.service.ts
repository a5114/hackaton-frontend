import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Product} from "../models/product";


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url : string = 'http://localhost:3000/products'

  constructor(private httpClient : HttpClient) { }

  post(product : Product): Observable<Product>{
    return this.httpClient.post<Product>(this.url,product);
  }

  get(): Observable<Product[]>{
    return this.httpClient.get<Product[]>(this.url);
  }

  getById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(`${this.url}/${id}`);

  }

  put(product : Product) : Observable<Product>{
    return this.httpClient.put<Product>(this.url+"/"+product.id,product);
  }

  delete(id:number){
    return this.httpClient.delete(this.url+"/"+id);
  }

}
