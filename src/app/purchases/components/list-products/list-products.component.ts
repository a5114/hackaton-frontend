import {Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {Product} from "../../models/product";
import {ProductItemComponent} from "../product-item/product-item.component";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit, OnDestroy {

  products !: Product[];
  productsSelected : Product[] = [];
  subscriptions: Subscription = new Subscription();
  @ViewChildren('child') children: QueryList<ProductItemComponent>;

  constructor(private service : ProductService) {

  }

  ngOnInit(): void {
    const sub1$ = this.service.get().subscribe((data) => {
      this.products=data;
    });
    this.subscriptions.add(sub1$);
  }

  toggleProducts(p: Product): boolean{
    let flag = false;
    let index !: number;
    this.productsSelected.forEach(prod =>{
      if (prod.id === p.id){
        flag = true;
        index = this.productsSelected.indexOf(prod);
      }
    });
    if (flag){
      this.productsSelected.splice(index, 1);
    }else{
      this.productsSelected.push(p);
    }
    return flag;
  }

  quitProducts(p: Product){
    this.children.forEach( c => {
      if (c.product.id === p.id){
        c.selected = false;
        this.toggleProducts(p);
      }
    });
  }

  ngOnDestroy () {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }
}
