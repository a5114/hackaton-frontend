import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { IProduct } from 'src/app/products/models/product';
import { ProductsService } from 'src/app/products/services/products.service';
import { IProvider } from 'src/app/providers/models/provider';
import { ProviderService } from 'src/app/providers/services/provider.service';
import { User } from 'src/app/shared/models/User';
import { PurchaseOrder } from '../../models/purchase-order';
import { PurchaseOrderService } from '../../services/purchase-order.service';

@Component({
  selector: 'app-purchase-order-detail',
  templateUrl: './purchase-order-detail.component.html',
  styleUrls: ['./purchase-order-detail.component.css']
})
export class PurchaseOrderDetailComponent implements OnInit {
  purchase_order!: PurchaseOrder;
  purchaseId !: number;
  purchaseForm!: FormGroup;
  listProduct: IProduct[]=[];
  purchase_order_prueba!: PurchaseOrder;
  totalAmount:number=0;
  user:User;
  provider!:IProvider;

  constructor(private purchase_orderService: PurchaseOrderService, private router: Router, private builder: FormBuilder,
              private route: ActivatedRoute,private productService:ProductsService,private authService:AuthService,private providerService:ProviderService) { }

  ngOnInit(): void {    
    const routeParams = this.route.snapshot.paramMap;
    this.purchaseId = Number(routeParams.get('purchaseId'));

    if (this.purchaseId != null) {
      this.purchase_orderService.getPurchaseOrderById(this.purchaseId).subscribe(resp => {
        this.purchase_order = resp
        this.authService.getUserById(resp.userId).subscribe(resp=>{
           this.user=resp;
        });
        for (let numero of this.purchase_order.productsId){
          this.productService.getProductById(numero).subscribe(resp=>{
            this.listProduct.push(resp);
            this.providerService.getProviderById(resp.providerId).subscribe(resp=>{
              this.provider=resp;
            });
          }); 
        }
      });
    }

  }
}