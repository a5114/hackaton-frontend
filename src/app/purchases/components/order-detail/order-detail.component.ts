import { Component, OnInit } from '@angular/core';
import {SelectProductService} from "../../services/select-product.service";
import {TemporalOrder} from "../../models/temporal-order";
import {Router} from "@angular/router";
import {ProductService} from "../../services/product.service";
import {PurchaseOrderService} from "../../services/purchase-order.service";
import {Product} from "../../models/product";
import {PurchaseOrder} from "../../models/purchase-order";
import {ToastrService} from "ngx-toastr";
import {Subscription} from "rxjs";
import {AuthService} from "../../../auth/services/auth.service";

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {

  temporalOrder!: TemporalOrder;
  products !: Product[];
  amount!: number;
  subscriptions: Subscription = new Subscription();

  constructor(private selectedProduct: SelectProductService,
              private router: Router,
              private productService: ProductService,
              private purchaseOrderService: PurchaseOrderService,
              private toast: ToastrService,
              private authService: AuthService) { }

  ngOnInit(): void {
    this.temporalOrder = this.selectedProduct.selectedProduct;
    if (this.temporalOrder === undefined) {
      this.router.navigate(['/app/purchases']);
    }

    this.products = [];

    this.amount = 0;

    this.temporalOrder.productsId.forEach((id)=>{
      this.subscriptions.add(this.productService.getById(id).subscribe((product)=>{
        this.products.push(product)
        this.amount = this.amount + Number( product.price) ;
      }));
    });

  }

  pagar(){
    let purchaseOrder = new PurchaseOrder();
    purchaseOrder.userId = this.authService.user.id;
    purchaseOrder.totalAmount= this.amount;
    purchaseOrder.productsId = this.temporalOrder.productsId;
    purchaseOrder.status = "COMPLETED";
    this.subscriptions.add(this.purchaseOrderService.post(purchaseOrder).subscribe(resp => {
      this.toast.success('Purchase Order registered successfully!!', '');
      this.selectedProduct.selectedProduct = undefined;
      this.router.navigate(["/app/purchases"])
    }));
  }

  goBack () {
    this.router.navigate(['/app/purchases']);
  }

  ngOnDestroy () {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }
}
