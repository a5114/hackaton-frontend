import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { User } from 'src/app/shared/models/User';
import { PurchaseOrder } from '../../models/purchase-order';
import { PurchaseOrderService } from '../../services/purchase-order.service';

@Component({
  selector: 'app-purchase-order-detail-list',
  templateUrl: './purchase-order-detail-list.component.html',
  styleUrls: ['./purchase-order-detail-list.component.css']
})
export class PurchaseOrderDetailListComponent implements OnInit {
  list: PurchaseOrder[]=[];
  constructor(private purchase_orderService:PurchaseOrderService,private userService:AuthService) { }
   user:User;
   purchase:PurchaseOrder;
  ngOnInit(): void {
    this.listPurchase_orders();
  }

  listPurchase_orders() {
    this.purchase_orderService.getPurchaseOrderList().subscribe(data => {
      for(let i in data){
        this.user=this.userService.user;
          if(data[i].userId==this.user.id){
            this.list.push(data[i]);
          }
        }
    })
  }
  
}
