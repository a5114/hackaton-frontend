import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderDetailListComponent } from './purchase-order-detail-list.component';

describe('PurchaseOrderDetailListComponent', () => {
  let component: PurchaseOrderDetailListComponent;
  let fixture: ComponentFixture<PurchaseOrderDetailListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseOrderDetailListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
