import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {ListProductsComponent} from "../list-products/list-products.component";

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  selected:boolean = false;

  @Input('p') product!: Product;
  constructor(private listProducts: ListProductsComponent) { }

  ngOnInit(): void {
  }

  change(){
    this.selected = !this.selected;
    this.listProducts.toggleProducts(this.product);
  }
}
