import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Product} from "../../models/product";
import {TemporalOrderService} from "../../services/temporal-order.service";
import {TemporalOrder} from "../../models/temporal-order";
import {AuthService} from "../../../auth/services/auth.service";
import {SelectProductService} from "../../services/select-product.service";
import {Router} from "@angular/router";

@Component({
  selector: 'modal-selected-products',
  templateUrl: './selected-products.component.html',
  styleUrls: ['./selected-products.component.css']
})
export class SelectedProductsComponent implements OnInit {
  closeResult = '';

  @Input('selectedProducts') selectedProducts : Product[];

  @Output() event = new EventEmitter<Product>()

  constructor(
    private modalService: NgbModal,
    private orderService: TemporalOrderService,
    private authService: AuthService,
    private selected: SelectProductService,
    private  router: Router) {}

  ngOnInit(): void {
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  saveOrder(){

    let ids : number[] = [];
    this.selectedProducts.forEach((selected)=>{
      ids.push(selected.id);
    })
    let orderTemp : TemporalOrder = {
      productsId: ids,
      userId: this.authService.user.id
    };

    this.selected.selectedProduct=orderTemp;
    this.router.navigate(["/app/purchases/orderDetail"])
  }

  quitarProducto(product: Product){
    this.event.emit(product);
  }
}
