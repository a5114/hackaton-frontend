export class PurchaseOrder {
  id?: number;
  productsId: number [];
  userId: number;
  totalAmount: number;
  status?: string;

  constructor() {
  }

}
