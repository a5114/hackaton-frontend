export interface Product {
  id?: number;
  providerId: number;
  name: string;
  code: string;
  price: number;
  description?: string;
  image: string;
}
