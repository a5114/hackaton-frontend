import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OrderDetailComponent} from "./components/order-detail/order-detail.component";
import {RoleGuard} from "../core/guards/role.guard";
import {UserRole} from "../shared/enums/user-role.enum";
import { ListProductsComponent } from './components/list-products/list-products.component';
import { PurchaseOrderDetailListComponent } from './components/purchase-order-detail-list/purchase-order-detail-list.component';
import { PurchaseOrderDetailComponent } from './components/purchase-order-detail/purchase-order-detail.component';

const routes: Routes = [
  {
    path: "",
    canActivate: [RoleGuard],
    component: ListProductsComponent,
    data: {
      role: [
        UserRole.SYSTEM,
        UserRole.REGULAR
      ]
    }
  }
  ,{
    path: "orderDetail",
    canActivate: [RoleGuard],
    component: OrderDetailComponent,
    data: {
      role: [
        UserRole.SYSTEM,
        UserRole.REGULAR
      ]
    }
  },{
    path: 'orderDetail/list',
    component: PurchaseOrderDetailListComponent,
    canActivate: [RoleGuard],
    data: {
      role: [
        UserRole.ADMIN,
        UserRole.REGULAR
      ]
    }
  },
  {
    path: 'edit-purchase/:purchaseId',
    component: PurchaseOrderDetailComponent,
     canActivate: [RoleGuard],
      data: {
      role: [
        UserRole.ADMIN,
        UserRole.REGULAR
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchasesRoutingModule { }
