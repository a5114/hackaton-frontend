import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchasesRoutingModule } from './purchases-routing.module';
import { ListProductsComponent } from './components/list-products/list-products.component';
import { SelectedProductsComponent } from './components/selected-products/selected-products.component';
import { CoreModule } from '../core/core.module';
import { ProductItemComponent } from './components/product-item/product-item.component';
import { OrderDetailComponent } from './components/order-detail/order-detail.component';
import { PurchaseOrderDetailListComponent } from './components/purchase-order-detail-list/purchase-order-detail-list.component';
import { PurchaseOrderDetailComponent } from './components/purchase-order-detail/purchase-order-detail.component';


@NgModule({
  declarations: [
    ListProductsComponent,
    SelectedProductsComponent,
    ProductItemComponent,
    OrderDetailComponent,
    PurchaseOrderDetailListComponent,
    PurchaseOrderDetailComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    PurchasesRoutingModule
  ]
})
export class PurchasesModule { }
