import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProduct } from '../../models/product';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-product-edit-form',
  templateUrl: './product-edit-form.component.html',
  styleUrls: ['./product-edit-form.component.css']
})
export class ProductEditFormComponent implements OnInit {
  productForm!: FormGroup;
  product: IProduct;
  productId!: number;
  idProvider!: number;  

  constructor(private productService: ProductsService, private route: ActivatedRoute, private router: Router,
              private builder: FormBuilder, private toast: ToastrService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.productId = Number(routeParams.get('productId'));    
    this.idProvider = Number(routeParams.get('idProvider'));

    this.productForm = this.builder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      price: [0, Validators.required],
      description: [''],
      providerId: [],
    });

    if (this.productId != null) {
      this.productService.getProductById(this.productId).subscribe(resp => {
        this.product = resp;
        this.loadData();
      });
    }
  }

  editProduct() {
    const product: IProduct = this.productForm.value;
    this.productService.editProduct(product, this.productId).subscribe(resp => {
      this.toast.success('Product successfully edited', '');
      this.router.navigate(['/app/products/provider/', this.idProvider]);
    });
  }
 
  loadData() {
    this.name.setValue(this.product.name);
    this.code.setValue(this.product.code);
    this.price.setValue(this.product.price);
    this.providerId.setValue(this.product.providerId);
    this.description.setValue(this.product.description);
  }

  get name(): FormControl {
    return this.productForm.get('name') as FormControl;
  }
  get code(): FormControl {
    return this.productForm.get('code') as FormControl;
  }
  get price(): FormControl {
    return this.productForm.get('price') as FormControl;
  }
  get providerId(): FormControl {
    return this.productForm.get('providerId') as FormControl;
  }
  get description(): FormControl {
    return this.productForm.get('description') as FormControl;
  }
  get image(): FormControl {
    return this.productForm.get('image') as FormControl;
  }
}