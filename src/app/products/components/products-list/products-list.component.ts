import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from '../../models/product';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  listProducts!: IProduct[];
  idProvider: number;
  constructor(private productService: ProductsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.idProvider = Number(routeParams.get('idProvider'));
    if (this.idProvider != null) {
      this.getListProduct();
    } else {
      this.router.navigate(['/app/home']);
    }
  }

  getListProduct() {
    this.productService.getProductListByProvider(this.idProvider).subscribe( data => {
      this.listProducts = data;
    });
  }
}
