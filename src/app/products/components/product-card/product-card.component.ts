import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { IProduct } from '../../models/product';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  @Input() product: IProduct;
  @Output() delete: EventEmitter<boolean> = new EventEmitter<boolean> ();
  constructor(private productService: ProductsService, private toast: ToastrService) { }

  ngOnInit(): void {
  }

  deleteProduct(id: number) {
    var r = confirm ("Are you sure to remove the product?");
    if (r == true) {
      this.productService.deleteProduct(id).subscribe(resp => {        
        this.toast.success('Product successfully removed', '');
        this.delete.emit(true);
      });
    }
  }

}
