import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProduct } from '../../models/product';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  productForm!: FormGroup;
  idProvider!: number;
  fileToUpload: File;
  fileData: string;

  constructor(private productService: ProductsService, private route: ActivatedRoute, private router: Router,
              private builder: FormBuilder, private toast: ToastrService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.idProvider = Number(routeParams.get('idProvider'));
    if (this.idProvider == null) {
      this.router.navigate(['/app/home']);
    }

    this.productForm = this.builder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      price: [0, Validators.required],
      description: [''],
      image:[, Validators.required],
      providerId: [this.idProvider]
    });
  }

  addProduct() {
    const product: IProduct = this.productForm.value;
    this.convertFileToString().then( (res) => {
      product.image = res.toString();
      this.productService.addProduct(product).subscribe(resp => {
        this.toast.success('Successfully created product', '');
        this.router.navigate(['/app/products/provider/', this.idProvider]);
      });
    }).catch ((error) => {
      this.toast.error('Error creating the product, please try again', '');
    });
  }

  fileChangeEvent(e) {
    this.fileToUpload = e.target.files[0];
  }

  getBase64(file) {
    return new Promise(function (resolve, reject) {
         let reader = new FileReader();
         reader.onload = function () { resolve(reader.result); };
         reader.onerror = reject;
         reader.readAsDataURL(file);
     });
 }

  async convertFileToString () {
    let promise = this.getBase64(this.fileToUpload);
    return await promise;
  }

  get name(): FormControl {
    return this.productForm.get('name') as FormControl;
  }
  get code(): FormControl {
    return this.productForm.get('code') as FormControl;
  }
  get price(): FormControl {
    return this.productForm.get('price') as FormControl;
  }
  get image(): FormControl {
    return this.productForm.get('image') as FormControl;
  }
  get description(): FormControl {
    return this.productForm.get('description') as FormControl;
  }
}
