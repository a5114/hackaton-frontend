export interface IProduct {
    id: number,
    name: string,
    code: string,
    providerId: number,
    image: string
    price: number,
    description: string
}