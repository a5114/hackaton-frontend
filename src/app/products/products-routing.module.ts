import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../core/guards/role.guard';
import { UserRole } from '../shared/enums/user-role.enum';
import { ProductEditFormComponent } from './components/product-edit-form/product-edit-form.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ProductsListComponent } from './components/products-list/products-list.component';

const routes: Routes = [  
  { path: 'provider/:idProvider',
    component: ProductsListComponent,
    canActivate: [RoleGuard],
    data: {
      role: [
        UserRole.ADMIN
      ]
    }
  },
  { path: 'new-product/:idProvider',
    component: ProductFormComponent,
    canActivate: [RoleGuard],
    data: {
      role: [
        UserRole.ADMIN
      ]
    }
  },
  { path: 'edit-product/:idProvider/:productId',
    component: ProductEditFormComponent,
    canActivate: [RoleGuard],
    data: {
      role: [
        UserRole.ADMIN
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
