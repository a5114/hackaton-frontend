import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProduct } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private url = 'http://localhost:3000/products';

  constructor(private httpClient: HttpClient) { }

  getProductList(): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>(this.url).pipe(
      map((data) => data)
    );
  }

  getProductListByProvider(idProvider: number): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>(this.url + "?providerId=" + idProvider).pipe(
      map((data) => data)
    );
  }

  getProductById(id: number): Observable<IProduct> {
    return this.httpClient.get<IProduct>(this.url + "/" + id).pipe(
      map((data) => data)
    );
  }

  addProduct(product: IProduct) {
    return this.httpClient.post(this.url, product);
  }

  editProduct(product: IProduct, id: number) {
    return this.httpClient.patch(this.url + "/" + id, product);
  }

  deleteProduct(id: number) {
    return this.httpClient.delete(this.url + "/" + id);
  }
}
