import { Component, OnInit } from '@angular/core';
import { IPromotion } from '../promotions/models/promotion';
import { StatusPromotion } from '../promotions/models/status-promotion';
import { PromotionService } from '../promotions/services/promotion.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  list!: IPromotion[];
  constructor(private promotionService: PromotionService) { }

  ngOnInit(): void {
    this.activePromotions();
  }

  activePromotions(){
    this.promotionService.getPromotionsByStatus(StatusPromotion.ACTIVE).subscribe(data =>{
      this.list = data;
    });
  }
}
