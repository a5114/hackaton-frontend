import { Component, Input, OnInit } from '@angular/core';
import { IMenuItemsDto } from '../menu-items-dto';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() public menu: IMenuItemsDto[];
  constructor() { }

  ngOnInit(): void {
  }
}

