export class IMenuItemsDto {
    title: string;
    path?: string;
    icon: string;
}