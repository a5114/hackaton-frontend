import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IMenuItemsDto } from '../menu-items-dto';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  constructor(private httpClient: HttpClient) { }

  getMenuItems(): Observable<IMenuItemsDto[]> {
    return this.httpClient.get<IMenuItemsDto[]>('http://localhost:3000/menu').pipe(
      map((menu) => menu)
    );
  }
}
