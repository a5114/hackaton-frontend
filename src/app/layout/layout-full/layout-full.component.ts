import { Component, OnInit } from '@angular/core';
import { IMenuItemsDto } from 'src/app/layout/menu-items-dto';
import { MenuService } from 'src/app/layout/services/menu.service';

@Component({
  selector: 'app-layout-full',
  templateUrl: './layout-full.component.html',
  styleUrls: ['./layout-full.component.css']
})
export class LayoutFullComponent implements OnInit {
  public isCollapsed = false;
  
  public menuItems: IMenuItemsDto[];
  title = 'hackaton-frontend';

  constructor(private menuService: MenuService) { }

  ngOnInit(): void {    
    this.menuService.getMenuItems().subscribe( resp => {
      this.menuItems = resp;
    });
  }
}
