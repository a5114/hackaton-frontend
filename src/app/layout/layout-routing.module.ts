import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../core/guards/auth.guard';
import { HomeComponent } from '../home/home.component';
import { LayoutFullComponent } from './layout-full/layout-full.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutFullComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {
          path: 'providers',
          canActivateChild: [AuthGuard],
          loadChildren: () => import("../providers/providers.module").then(m => m.ProvidersModule)
      },
      {
          path: 'products',
          canActivateChild: [AuthGuard],
          loadChildren: () => import("../products/products.module").then(m => m.ProductsModule)
      },
      {
        path: 'purchases',
        canActivateChild: [AuthGuard],
        loadChildren: () => import("../purchases/purchases.module").then(m => m.PurchasesModule)
      },
      {
        path: 'promotions',
        canActivateChild: [AuthGuard],
        loadChildren: () => import("../promotions/promotions.module").then(m => m.PromotionsModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
