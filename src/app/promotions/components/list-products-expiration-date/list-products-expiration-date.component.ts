import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/products/services/products.service';
import { IPromotion } from '../../models/promotion';
import { StatusPromotion } from '../../models/status-promotion';
import { PromotionService } from '../../services/promotion.service';

@Component({
  selector: 'app-list-products-expiration-date',
  templateUrl: './list-products-expiration-date.component.html',
  styleUrls: ['./list-products-expiration-date.component.css']
})
export class ListProductsExpirationDateComponent implements OnInit {

  public list!: IPromotion[];
  id!: number;
  constructor(private promotionService: PromotionService, private productService: ProductsService) { }

  ngOnInit(): void {
    this.listPromotions();
  }

  listPromotions() {
    this.promotionService.getPromotionsByStatus(StatusPromotion.ACTIVE).subscribe( resp => {
      this.list = this.promotionService.filterPromotionsByExpirationDate(resp);
    });
  } 
  
}
