import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductsExpirationDateComponent } from './list-products-expiration-date.component';

describe('ListProductsExpirationDateComponent', () => {
  let component: ListProductsExpirationDateComponent;
  let fixture: ComponentFixture<ListProductsExpirationDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListProductsExpirationDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductsExpirationDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
