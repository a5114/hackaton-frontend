import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from 'src/app/products/models/product';
import { ProductsService } from 'src/app/products/services/products.service';
import { IPromotion } from '../../models/promotion';
import { PromotionService } from '../../services/promotion.service';

@Component({
  selector: 'app-products-promotion',
  templateUrl: './products-promotion.component.html',
  styleUrls: ['./products-promotion.component.css']
})
export class ProductsPromotionComponent implements OnInit {
  listProducts: IProduct[]=[];
  promotionId: number;
  constructor(private productService: ProductsService, private promotionService: PromotionService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.promotionId = Number(routeParams.get('promotionId'));
    if (this.promotionId != null) {
      this.getListProduct();
    }
  }

  getListProduct() {
    this.promotionService.getPromotionById(this.promotionId).subscribe(data =>{
      data.products.forEach(element => {
        this.productService.getProductById(element).subscribe(response =>{
          this.listProducts.push(response);
        })
      });
    });
  }
}
