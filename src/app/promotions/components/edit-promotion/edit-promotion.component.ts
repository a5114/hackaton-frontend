import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProduct } from 'src/app/products/models/product';
import { ProductsService } from 'src/app/products/services/products.service';
import { IPromotion } from '../../models/promotion';
import { PromotionService } from '../../services/promotion.service';

@Component({
  selector: 'app-edit-promotion',
  templateUrl: './edit-promotion.component.html',
  styleUrls: ['./edit-promotion.component.css']
})
export class EditPromotionComponent implements OnInit {
  public promotion!: IPromotion;
  public promotionId !: number;
  public promotionForm !: FormGroup;
  productList !: IProduct[];

  constructor(private promotionService: PromotionService, private productService: ProductsService,
    private router: Router, private builder: FormBuilder,
    private route: ActivatedRoute, private toast: ToastrService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.promotionId = Number(routeParams.get('promotionId'));
    this.promotionForm = this.builder.group({
      name: ['', Validators.required],
      discount: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      products: this.builder.array([this.builder.control('', [Validators.required])]),
    },{
      validator: this.dateLessThan('startDate', 'endDate')
    });

    if (this.promotionId != null) {
      this.promotionService.getPromotionById(this.promotionId).subscribe(resp => {
        this.promotion = resp;
        this.loadData();
      });
    }
    this.productService.getProductList().subscribe(data =>{
      this.productList = data;
    });
  }

  editPromotion() {
    const promo: IPromotion = this.promotionForm.value;
    this.promotionService.editPromotion(promo, this.promotionId).subscribe(resp => {
      this.toast.success('Promotion successfully edited', '');
      this.router.navigate(['/app/promotions']);
    });
  }

  dateLessThan(start: string, end: string) {
    return (group: FormGroup): void => {
      let activation = group.controls[start];
      let expiration = group.controls[end];
      if (activation.value > expiration.value) {
        expiration.setErrors({ dateLessThan: true});
      } else {
        expiration.setErrors(null);
      }
    };
  }
  
  loadData() {
    this.name.setValue(this.promotion.name);
    this.startDate.setValue(this.promotion.startDate);
    this.endDate.setValue(this.promotion.endDate);
    this.discount.setValue(this.promotion.discount);
    this.setProducts(this.promotion.products);
  }
  
  private setProducts(products: number[]): void {
    let i = 0;
    for (const product of products) {
      if (i === 0) {
        this.products.setValue([product]);
      } else {
        this.products.push(this.builder.control(product));
      }
      i++;
    }
  }

  addProduct(){
    this.products.push(this.builder.control(''));
  }

  deleteProduct(id: number){
    if (this.products.length > 1) {
      this.products.removeAt(id);
    }
  }

  get name(): FormControl {
    return this.promotionForm.get('name') as FormControl;
  }
  get startDate(): FormControl {
    return this.promotionForm.get('startDate') as FormControl;
  }
  get endDate(): FormControl {
    return this.promotionForm.get('endDate') as FormControl;
  }
  get discount(): FormControl {
    return this.promotionForm.get('discount') as FormControl;
  }
  get products(): FormArray {
    return this.promotionForm.get('products') as FormArray;
  }
}
