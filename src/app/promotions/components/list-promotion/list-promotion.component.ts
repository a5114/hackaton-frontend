import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { IPromotion } from '../../models/promotion';
import { StatusPromotion } from '../../models/status-promotion';
import { PromotionService } from '../../services/promotion.service';

@Component({
  selector: 'app-list-promotion',
  templateUrl: './list-promotion.component.html',
  styleUrls: ['./list-promotion.component.css']
})
export class ListPromotionComponent implements OnInit {
  public listStatus: StatusPromotion[];
  public list!: IPromotion[];
  public active= StatusPromotion.ACTIVE;
  statusList: StatusPromotion[] = [];
  public formStatus: FormControl = new FormControl();

  constructor(private promotionService: PromotionService) { }

  ngOnInit(): void {
    this.listPromotions();
    this.listStatus = [StatusPromotion.ACTIVE, StatusPromotion.PAST];
    this.formStatus.valueChanges.subscribe((value) => {
        this.loadDataByStatus(value);
    });
  }

  listPromotions() {
    this.promotionService.getPromotions().subscribe(data => {
      this.list = data;
    });
  }

  deletePromotion(id: number) {
    this.promotionService.deletePromotion(id).subscribe(resp => {
      this.listPromotions();
    });
  }

  loadDataByStatus(status: StatusPromotion ) {
    this.promotionService.getPromotionsByStatus(status).subscribe(data =>{
      this.list = data;
    });
  }

  changeStatus(id: number){
    this.promotionService.changeStatus(id, StatusPromotion.PAST).subscribe(resp => {
      this.listPromotions();
    });
  }
}
