import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProduct } from 'src/app/products/models/product';
import { ProductsService } from 'src/app/products/services/products.service';
import { IPromotion } from '../../models/promotion';
import { StatusPromotion } from '../../models/status-promotion';
import { PromotionService } from '../../services/promotion.service';

@Component({
  selector: 'app-register-promotion',
  templateUrl: './register-promotion.component.html',
  styleUrls: ['./register-promotion.component.css']
})
export class RegisterPromotionComponent implements OnInit {

  public promotionForm !: FormGroup;
  productList !: IProduct[];

  constructor(private promotionService: PromotionService, private productService: ProductsService,
    private router: Router, private builder: FormBuilder, private toast: ToastrService) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.promotionForm = this.builder.group({
      name: ['', Validators.required],
      discount: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      products: this.builder.array([this.builder.control('', [Validators.required])]),
    },{
      validator: this.dateLessThan('startDate', 'endDate')
    });
    this.productService.getProductList().subscribe(data =>{
      this.productList = data;
    });
  }

  addPromotion(){
    const promotion: IPromotion = this.promotionForm.value;
    promotion.status = StatusPromotion.ACTIVE;
    this.promotionService.createPromotion(promotion).subscribe(resp => {
      this.toast.success('Successfully created promotion', '');
      this.router.navigate(['/app/promotions'])
    });
  }

  addProduct(){
    this.products.push(this.builder.control(''));
  }

  deleteProduct(id: number){
    if (this.products.length > 1) {
      this.products.removeAt(id);
    }
  }

  dateLessThan(start: string, end: string) {
    return (group: FormGroup): void => {
      let activation = group.controls[start];
      let expiration = group.controls[end];
      if (activation.value > expiration.value) {
        expiration.setErrors({ dateLessThan: true});
      } else {
        expiration.setErrors(null);
      }
    };
  }

  get name(): FormControl {
    return this.promotionForm.get('name') as FormControl;
  }
  get startDate(): FormControl {
    return this.promotionForm.get('startDate') as FormControl;
  }
  get endDate(): FormControl {
    return this.promotionForm.get('endDate') as FormControl;
  }
  get discount(): FormControl {
    return this.promotionForm.get('discount') as FormControl;
  }
  get products(): FormArray {
    return this.promotionForm.get('products') as FormArray;
  }
}
