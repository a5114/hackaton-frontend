import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionsRoutingModule } from './promotions-routing.module';
import { ListPromotionComponent } from './components/list-promotion/list-promotion.component';
import { RegisterPromotionComponent } from './components/register-promotion/register-promotion.component';
import { ListProductsExpirationDateComponent } from './components/list-products-expiration-date/list-products-expiration-date.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditPromotionComponent } from './components/edit-promotion/edit-promotion.component';
import { ProductsPromotionComponent } from './components/products-promotion/products-promotion.component';


@NgModule({
  declarations: [
    ListPromotionComponent,
    RegisterPromotionComponent,
    ListProductsExpirationDateComponent,
    EditPromotionComponent,
    ProductsPromotionComponent
  ],
  imports: [
    CommonModule,
    PromotionsRoutingModule,
    ReactiveFormsModule
  ]
})
export class PromotionsModule { }
