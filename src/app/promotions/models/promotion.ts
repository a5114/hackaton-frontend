import { IProduct } from "src/app/products/models/product";
import { StatusPromotion } from "./status-promotion";

export interface IPromotion {
    id: number;
    status: StatusPromotion,
    name: string,
    startDate: Date,
    endDate: Date,
    discount: string,
    products: number[],
  }