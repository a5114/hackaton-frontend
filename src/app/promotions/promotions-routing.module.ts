import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListProductsExpirationDateComponent } from './components/list-products-expiration-date/list-products-expiration-date.component';
import { ListPromotionComponent } from './components/list-promotion/list-promotion.component';
import { RegisterPromotionComponent } from './components/register-promotion/register-promotion.component';
import { EditPromotionComponent } from './components/edit-promotion/edit-promotion.component';
import { ProductsPromotionComponent } from './components/products-promotion/products-promotion.component';

const routes: Routes = [
  {
    path: 'register',
    component: RegisterPromotionComponent,
  },
  {
    path: '',
    component: ListPromotionComponent,
  },
  {
    path: 'productExpirationlist',
    component: ListProductsExpirationDateComponent,
  },
  {
    path: 'edit/:promotionId',
    component: EditPromotionComponent,
  },
  {
    path: 'products/:promotionId',
    component: ProductsPromotionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionsRoutingModule { }
