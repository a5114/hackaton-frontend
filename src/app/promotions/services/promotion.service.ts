import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPromotion } from '../models/promotion';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatusPromotion } from '../models/status-promotion';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {
  private baseURL: string = "http://localhost:3000/promotions";
  constructor(private http: HttpClient) { }

  getPromotions(): Observable<IPromotion[]>{
    return this.http.get(this.baseURL).pipe(
      map(resp => {
        return resp as IPromotion[];
      })
    );
  }

  getPromotionsByStatus(status: StatusPromotion): Observable<IPromotion[]>{
    return this.http.get(this.baseURL+"?status="+status).pipe(
      map(resp => {
        return resp as IPromotion[];
      })
    );
  }
  changeStatus(id: number, newStatus: StatusPromotion){
    return this.http.patch(this.baseURL+ "/"+ id, {status: newStatus});
  }

  getPromotionById(id: number): Observable<IPromotion> {
    return this.http.get<IPromotion>(this.baseURL + "/" + id).pipe(
      map((data) => data)
    );
  }

  createPromotion(promo: IPromotion): Observable<any>{
    return this.http.post(this.baseURL, promo );
  }

  editPromotion(promo: IPromotion, id: number) {
    return this.http.patch(this.baseURL + "/" + id, promo);
  }

  deletePromotion(id: number) {
    return this.http.delete(this.baseURL + "/" + id);
  }

  filterPromotionsByExpirationDate(lista:IPromotion[]): IPromotion[] {
    let list: IPromotion[] = [];
    let dateNow: Date = new Date(Date.now());
    lista.forEach( promotion => {
      let datePromotion = new Date(promotion.endDate);
      if (datePromotion <= dateNow) {
        list.push(promotion);
      }
    });
    list.sort( function (a, b) {
      let aDate = new Date(a.endDate);
      let bDate = new Date(b.endDate);
      if (aDate < bDate) {
        return -1;
      } else if (aDate > bDate) {
        return 1;
      } else {
        return 0;
      }
    });
    return list;
  }

}
