import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;

  subscriptions: Subscription = new Subscription();

  constructor(private authService: AuthService, private router: Router, private toast: ToastrService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm () {
    this.form = this.formBuilder.group({
      name: [, Validators.required],
      password: [, Validators.required]
    });
  }

  public login () {
    const sub$ = this.authService.login(this.name.value, this.password.value).subscribe(resp => {
      if (resp !== null && resp !== undefined) {
        this.authService.user = resp;
        this.router.navigate(['/app/home']);
      } else {
        this.toast.info(`Fields are incorrect, verify fields are correct.`, '');
      }
    });
    this.subscriptions.add(sub$);
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  get password(): FormControl {
    return this.form.get('password') as FormControl;
  }

}
