import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StorageConstant } from 'src/app/shared/enums/storage-constant.enum';
import { User } from 'src/app/shared/models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private base_url: string = 'http://localhost:3000/users';
  private _user: User;

  constructor(private http: HttpClient) {
  }

  public login (name: string, password: string): Observable<User> {
    const params: HttpParams = new HttpParams()
    .set('name', name)
    .set('password', password);
    return this.http.get<User[]>(`${this.base_url}`, {params}).pipe
    (map((response) => {
      return response[0];
    }));
  }

  public logout () {
    sessionStorage.removeItem(StorageConstant.USER);
    this._user = null;
  }

  public set user (user: User) {
    sessionStorage.setItem(StorageConstant.USER, JSON.stringify(user));
    this._user = user;
  }

  public get user (): User {
    if (this._user != undefined && this._user !== null) {
      return this._user;
    }
    if (sessionStorage.getItem(StorageConstant.USER) === undefined || sessionStorage.getItem(StorageConstant.USER) === null || sessionStorage.getItem(StorageConstant.USER) === '') {
      return null;
    }
    this._user = JSON.parse(sessionStorage.getItem(StorageConstant.USER));
    return this._user;
  }
  
  getUserById(id: number): Observable<User> {
    return this.http.get<User>(this.base_url + "/" + id).pipe(
      map((data) => data)
    );
  }
}
