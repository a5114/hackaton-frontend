import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectProductDirective } from './directives/ng-select-product.directive';



@NgModule({
  declarations: [
    NgSelectProductDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NgSelectProductDirective,
  ]
})
export class CoreModule { }
