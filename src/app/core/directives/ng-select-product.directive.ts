import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[ngSelectProduct]'
})
export class NgSelectProductDirective {

  @Input("ngSelectProduct") set ngSelectProduct(condition: boolean) {
    this.fun1(condition)
  }
  constructor(private elementRef: ElementRef) { }

    fun1(flag: boolean){
      if (flag){
        this.elementRef.nativeElement.classList.add("text-white");
        this.elementRef.nativeElement.classList.add("bg-success");
      }else {
        this.elementRef.nativeElement.classList.remove("text-white");
        this.elementRef.nativeElement.classList.remove("bg-success");
      }
    }
}
