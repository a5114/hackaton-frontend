import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { UserRole } from 'src/app/shared/enums/user-role.enum';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor (private authService: AuthService, private router: Router, private toast: ToastrService) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const roles = route.data['role'] as Array<UserRole>;
    let hasRole = false;
    if (this.authService.user.role == UserRole.SUPER_ADMIN) {
      return true;
    }
    if (roles.length > 0) {
      roles.forEach(rol => {
        if (this.authService.user.role == rol) {
          hasRole = true;
        }
      });
    }
    if (hasRole) {
      return true;
    }
    this.toast.warning(`User ${this.authService.user.name} does not have permissions for this resource`, '');
    this.router.navigate([''])
    return false;
  }
  
}
