import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./core/guards/auth.guard";

const routes: Routes = [
    {path: '', redirectTo: "/auth/login", pathMatch: "full"},
    {
        path: 'auth',
        loadChildren: () => import ('./auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'app',
        canActivateChild: [AuthGuard],
        loadChildren: () => import("./layout/layout.module").then(m => m.LayoutModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
